package token.service;

import lombok.Getter;

/*
  ####################
  # Rasmus (s205357) #
  ####################
*/
public class User {
    @Getter
    private final String id;

    public User(String id) {
        this.id = id;
    }
}
