package token.service;

import messaging.implementations.RabbitMqQueue;
/*
  #########################################
  # Rasmus (s205357) and Andrea (s233336) #
  #########################################
*/
public class StartUp {
    public static void main(String[] args) throws Exception {
        new StartUp().startUp();
    }

    private void startUp() throws Exception {
        System.out.println("startup");
        var mq = new RabbitMqQueue("rabbitMq");
        new TokenService(mq);
    }
}
